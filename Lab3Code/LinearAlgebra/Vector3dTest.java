/**
 * Author: Kevin Echenique Arroyo
 * Student #: 1441258
 */
package LinearAlgebra;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class Vector3dTest {
/**
 * Testing Get methods
 */
    @Test
    public void testGetMethods(){
        Vector3d vector = new Vector3d(5,4,3);
        assertEquals(5, vector.getX());
        assertEquals(4, vector.getY());
        assertEquals(3, vector.getZ());

    }
/**
 * Testing magnitude method
 */
    @Test
    public void testMagnitude(){
        Vector3d vector2 = new Vector3d(1, 1, 2);
        assertEquals(2.44, vector2.magnitude(), 2);
    }
/**
 * Testing dotProduct method
 */
    @Test
    public void testDotProduct(){
        Vector3d vector3 = new Vector3d(2,4,6);
        double dotProduct = vector3.dotProduct(new Vector3d(1,2,3));
        assertEquals(28, dotProduct);
        
    }
/**
 * Testing add method
 */
    @Test
    public void testAdd(){
        Vector3d vector4 = new Vector3d(2,4,6);
        Vector3d vectorResult = vector4.add(new Vector3d(2,3,4));
        assertEquals(4, vectorResult.getX());
        assertEquals(7, vectorResult.getY());
        assertEquals(10, vectorResult.getZ());
    }
}
