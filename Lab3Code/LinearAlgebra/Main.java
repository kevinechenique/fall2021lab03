/**
 * Author: Kevin Echenique Arroyo
 * Student #: 1441258
 */
package LinearAlgebra;

public class Main {
  
    public static void main(String[]args){

        Vector3d vector = new Vector3d(2,4,6);

        double magnitude = vector.magnitude();
        double dotProduct = vector.dotProduct(new Vector3d(1,2,3));

        System.out.println(magnitude +"\n"+ dotProduct);

        Vector3d addVectors = vector.add(new Vector3d(2,3,4));
        System.out.println("\n" + addVectors.getX() +"\n"+ addVectors.getY() + "\n" + addVectors.getZ());

    }
}
