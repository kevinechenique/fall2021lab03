package LinearAlgebra;
/**
 * Author: Kevin Echenique Arroyo
 * Student #: 1441258
 */

public class Vector3d{
    
//Private fields
    private double x;
    private double y;
    private double z;
//Constructor
    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z= z;
    }
//Get Methods
    public double getX(){
        return this.x;
    }
    public double getY(){
        return this.y;
    }
    public double getZ(){
        return this.z;
    }
//Returns the magnitude using the given formula
    public double magnitude(){
        double magFormula = Math.sqrt((this.x*this.x)+(this.y*this.y)+(this.z*this.z));
        return magFormula;
    }
//Returns the dot product of the given Vector3d values
    public double dotProduct(Vector3d values){
        double dotProductResult = ((this.x*values.getX())+(this.y*values.getY())+(this.z*values.getZ()));
        return dotProductResult;
    }
//Adds each respective value (x+x,y+y,z+z) and outputs a new Vector3d with the new values
    public Vector3d add(Vector3d values){
        double addX = this.x + values.getX();
        double addY = this.y + values.getY();
        double addZ = this.z + values.getZ();
        Vector3d newValues = new Vector3d(addX, addY, addZ);
        return newValues;
    }
}